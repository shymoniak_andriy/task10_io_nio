package com.shymoniak.controller;

import com.shymoniak.model.*;

import java.io.File;

/**
 * + Create Ship with Droids. Serialize and deserialize them. Use transient.
 *
 * + Compare reading and writing performance of usual and buffered reader for
 * 200 MB file. Compare performance of buffered reader with different buffer
 * size (e.g. 10 different size).
 *
 * - Write your implementation of InputStream with capability of push read data
 * back to the stream.
 *
 * - Write a program that reads a Java source-code file (you provide the file
 * name on the command line) and displays all the comments. Do not use
 * regular expression.
 *
 * + Write a program that displays the contents of a specific directory (file and
 * folder names + their attributes) with the possibility of setting the current
 * directory (similar to “dir” and “cd” command line commands).
 *
 * +- Try to create SomeBuffer class, which can be used for read and write data
 * from/to channel (Java NIO)
 *
 * - Write client-server program using NIO (+ if you want, other implementation
 * using IO). E.g. you have one server and multiple clients. A client can send
 * direct messages to other client.
 */
public class Controller {
    public void writeIntoFile(){
//        Writer writer = new Writer();
        FileCreator fileCreator = new FileCreator();
        fileCreator.createIfNotExist(FileCreator.file200Mb);
        fileCreator.createIfNotExist(FileCreator.file20Mb);
//        writer.fileOutputStream();
//        writer.fileWriter();
//        writer.bufferedFIleWriter();


//        Reader reader = new Reader();
//        reader.fileReader();
//        reader.bufferedReader();

        SerializationDeserialization sd = new SerializationDeserialization();
        sd.serializePeople();
        sd.deserializePeople();
    }

    public void printDirectoryFiles(){
        ShowContent showContent = new ShowContent();
        showContent.printFolderContent(new File("D:\\EPAM\\Homework\\"));
    }

    public void readWrite(){
        FileCreator fileCreator = new FileCreator();
        fileCreator.createIfNotExist(FileCreator.file200Mb);

        ReaderWriter rw = new ReaderWriter();
        rw.readAndWrite(FileCreator.file200Mb);
    }
}
