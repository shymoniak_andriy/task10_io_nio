package com.shymoniak.model;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReaderWriter {

    public void readAndWrite(File file){
        try {

            Path path = Paths.get(file.getPath());
            RandomAccessFile randomaccessfile = new RandomAccessFile(file, "rw");
            FileChannel fileChannel = randomaccessfile.getChannel();

            // Creates an empty ByteBuffer with a 1024 byte capacity
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            byteBuffer.putInt(5);
            byteBuffer.putInt(10);
            byteBuffer.putInt(55);
            byteBuffer.putInt(555);
            byteBuffer.flip();
            int bytesWritten = fileChannel.write(byteBuffer);
            System.out.println("number of bytes written : " + bytesWritten);



            int i = fileChannel.read(byteBuffer);
            while (i != -1) {
                System.out.print(i + " ");
                byteBuffer.flip();
                while (byteBuffer.hasRemaining()) {
                    System.out.print((char) byteBuffer.get() + " ");
                }
                byteBuffer.clear();
                i = fileChannel.read(byteBuffer);
            }

            fileChannel.close();
            randomaccessfile.close(); // closing RandomAccess file
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
