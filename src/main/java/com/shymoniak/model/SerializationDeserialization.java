package com.shymoniak.model;

import java.util.HashSet;
import java.util.Set;


public class SerializationDeserialization {

    private Set<Person> people;

    public SerializationDeserialization(){
        people = new HashSet<>();
        people.add(new Person("Sasha", "Shlyapik", 21));
        people.add(new Person("Oldman", "Borov", 60));
        people.add(new Person("Zhmyshenko", "Valera", 64));
        people.add(new Person("Denchik", "Sukhachev", 28));
        people.add(new Person("Dmitriy", "Urich", 32));
        people.add(new Person("Sergey", "Maslory", 20));
        people.add(new Person("Radrigez", "Poluvyalyj", 44));
    }

    public void serializePeople(){
        Writer writer = new Writer();
        for (Person person: people){
            writer.objectOutputStream(person);
        }
        writer.closeStreams();
    }

    public void deserializePeople(){
        Reader reader = new Reader();
        Set<Person> resultSet = new HashSet<>();
        Set<Object> objects = reader.objectInputStream();
        objects.stream().forEach(ob -> System.out.println(ob.toString()));
//        objects.stream().forEach(ob -> resultSet.add((Person) ob));
        resultSet.forEach(el -> System.out.println(el.toString()));
        reader.closeStreams();
    }
}
