package com.shymoniak.model;

import java.io.*;
import java.util.Date;
import java.util.GregorianCalendar;

public class Writer {
    private ObjectOutputStream objectOutputStream;
    private FileOutputStream fileOutputStream;

    {
        try {
            fileOutputStream = new FileOutputStream(FileCreator.file200Mb);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Took 8 minutes to write 20Mb into file
     */
    public void fileOutputStream() {
        Date start;
        Date finish;
        try (FileOutputStream fileOutputStream = new FileOutputStream(FileCreator.file20Mb)) {
            int i = 0;
            start = new GregorianCalendar().getTime();
            while (FileCreator.file20Mb.length() < FileCreator.FILE20_BYTES_LENGTH) {
                i++;
                fileOutputStream.write(0);
                System.out.println(i);
            }
            finish = new GregorianCalendar().getTime();
            System.out.println("Program started working at " + start +
                    " and finished at " + finish);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Took 4 minutes to write 200Mb into file
     */
    public void fileWriter() {
        Date start;
        Date finish;

        try (FileWriter fileWriter = new FileWriter(FileCreator.file200Mb)) {
            int i = 0;
            start = new GregorianCalendar().getTime();
            while (FileCreator.file200Mb.length() < FileCreator.FILE200_BYTES_LENGTH) {
                i++;
                fileWriter.write("Random String FileWriter\t");
                System.out.println(i);
            }
            finish = new GregorianCalendar().getTime();
            System.out.println("Program started working at " + start +
                    " and finished at " + finish);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Took 2 minutes to write 200Mb into file
     */
    public void bufferedFIleWriter() {
        Date start;
        Date finish;

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FileCreator.file200Mb))) {
            int i = 0;
            start = new GregorianCalendar().getTime();
            while (FileCreator.file200Mb.length() < FileCreator.FILE200_BYTES_LENGTH) {
                i++;
                bufferedWriter.write("Random String BufferedWriter\t");
                bufferedWriter.newLine();
                System.out.println(i);
            }
            finish = new GregorianCalendar().getTime();
            System.out.println("Program started working at " + start +
                    " and finished at " + finish);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     *
     */
    public void objectOutputStream(Person object) {
        try {
            objectOutputStream.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void closeStreams(){
        try {
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
