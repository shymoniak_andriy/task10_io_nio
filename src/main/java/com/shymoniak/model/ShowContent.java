package com.shymoniak.model;

import java.io.File;

public class ShowContent {

    public void printFolderContent(File directory) {
        File[] filesList = directory.listFiles();
        for (File f : filesList) {
            if (f.isDirectory()) {
                System.out.println(f.getName());
            }
            if (f.isFile()) {
                System.out.println(f.getName());
            }
        }
    }
}
