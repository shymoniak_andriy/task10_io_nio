package com.shymoniak.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class FileCreator {
    public static final String FILE_PATH = "D:\\EPAM\\Homework\\homework13(IO_NIO)\\task10_io_nio\\src\\main\\resources\\";
    public static final File file200Mb = new File(FILE_PATH + "file200Mb.txt");
    public static final File file20Mb = new File(FILE_PATH + "file20Mb.txt");
    public static final long FILE200_BYTES_LENGTH = 200 * 1_000_000;
    public static final long FILE20_BYTES_LENGTH = 20 * 1_000_000;

    public void createIfNotExist(File file){
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            writer.print("");
            writer.close();
        }
    }


}
