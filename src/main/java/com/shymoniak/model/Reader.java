package com.shymoniak.model;

import java.io.*;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

public class Reader {
    private FileInputStream fileInputStream;
    private ObjectInputStream objectInputStream;

    public Reader () {
        try {
            fileInputStream = new FileInputStream(FileCreator.file200Mb);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            objectInputStream = new ObjectInputStream(fileInputStream);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /*
     * Took 8 minutes to read 20Mb from file
     */
    public void fileInputStream() {
        try (FileInputStream fileInputStream = new FileInputStream(FileCreator.file20Mb)) {
            int data = fileInputStream.read();
            while (data != -1) {
                System.out.println(data);
                data = fileInputStream.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /*
     * Took 9 minutes to read 200Mb from file
     */
    public void fileReader() {
        Date start;
        Date finish;
        try (FileReader fileReader = new FileReader(FileCreator.file200Mb)) {
            start = new GregorianCalendar().getTime();
            while (fileReader.read() != -1) {
                System.out.println(fileReader.read());
            }
            finish = new GregorianCalendar().getTime();
            System.out.println("Program started working at " + start +
                    " and finished at " + finish);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Took less than 1 second to read 200Mb from file using readline()
     * Took 9 minutes to read 200Mb from file using read()
     */
    public void bufferedReader() {
        Date start;
        Date finish;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(FileCreator.file200Mb))) {
            start = new GregorianCalendar().getTime();
            while (bufferedReader.read() != -1) {
                System.out.println(bufferedReader.read());
            }
            finish = new GregorianCalendar().getTime();
            System.out.println("Program started working at " + start +
                    " and finished at " + finish);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     *
     */
    public Set<Object> objectInputStream() {
        try {
            Set<Object> resultSet = new HashSet<>();
            while (fileInputStream.available() > 0) {
                resultSet.add(objectInputStream.readObject());
            }
            return resultSet;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void closeStreams(){
        try {
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
